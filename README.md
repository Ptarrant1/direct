# DIRECT
## Debian Incident Response Evidence Collection Thingy

The purpose of DIRECT is to gather a ton of evidence from a (Debian based) host for evidence collection in an Incident Response Nature.

## What's it do?
A BASH script to gather all kinds of evidence about a system. Primarily used to gather as much data about popped system as possible.
This will not only dump data about the machine, but also grab ssh logs, audit logs, and other useful information. Currently running processes, memory usage, network data and such. This will also collect memory as well
using Microsoft AVML tool. This will also create a volatility profile for you to use to analyze the memory dump.

## 3rd Pary tools used:
[Microsft AVML](https://github.com/microsoft/avml)

[Volatility](https://github.com/volatilityfoundation/volatility)


## Where is my evidence?
Your evidence will be placed in a new directory called DIRECT_mm-dd-yy-hh-mm. For example, direct_11_17_20_12_51. This directory is created as a child of the current working dir.

You can change the location of where your evidence is saved by changing the variable (OUTPUT_DIR) at the top of the script.


## Temp Files
Since this program uses 2 external tools, they will be placed in the TMP directory under names such as tmp_NTlmZmM0Yz. The reason for this is to not
put the files in some place were an attacker might be looking. The names generated are random/legit enough to where they might be over looked.


This is very useful for Incident Response. 

## Screenshot:
![screenshot](screenshot.png)