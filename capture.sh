#!/bin/bash

VERSION="1.0"
NOW=$(date +"%m_%d_%y_%H_%M")
OUTPUT_DIR="$(pwd)/direct_$NOW"

pretty_print()
{
    HEADER="\e[36m"
    INFO="\e[1m [+] INFO \t "
    ERROR="\e[91m [+] ERROR \t"
    YELLOW="\e[93m [+] INFO \t"
    NORMAL="\e[0m"
    

    if [ $1 == "HEADER" ];then 
        echo -e $HEADER $2 "\e[0m"

    elif [ $1 == "ERROR" ];then 
        echo -e $ERROR $2 "\e[0m"

    elif [ $1 == "YELLOW" ];then 
        echo -e $YELLOW $2 "\e[0m"
    
    elif [ $1 == "INFO" ];then 
        echo -e $INFO $2 "\e[0m"
    else
        echo $2
    fi

}

sudo_check()
{
    if [[ $EUID -ne 0 ]]; then
        echo "This script must be run as root" 
        exit 1
    fi
}

header()
{
    echo -e "\n"
    echo -e "==============================================================="
    pretty_print HEADER "DIRECT:"
    pretty_print HEADER "Debian Incident Response Evidence Collection Thingy"
    pretty_print HEADER "v$VERSION"
    echo -e "==============================================================="
    echo -e "\n"
}


header
sudo_check

pretty_print HEADER "Capturing System Data..."

mkdir -p {$OUTPUT_DIR/proc,$OUTPUT_DIR/mem,$OUTPUT_DIR/disk,$OUTPUT_DIR/net,$OUTPUT_DIR/user}

pretty_print INFO "Collecting Host Data..."

cat /etc/hostname > $OUTPUT_DIR/hostname.txt 2>&1
cat /etc/timezone > $OUTPUT_DIR/timezone.txt 2>&1
uname -a > $OUTPUT_DIR/uname.txt 2>&1
lsb_release -drc > $OUTPUT_DIR/lsb_release.txt 2>&1
cat /etc/os-release > $OUTPUT_DIR/os_release.txt 2>&1

#systemctl stuff
systemctl -l > $OUTPUT_DIR/systemctl_l.txt 2>&1
systemctl list-timers --all > $OUTPUT_DIR/systemctl_timers.txt 2>&1
systemctl list-unit-files > $OUTPUT_DIR/systemctl_unit_files.txt 2>&1

#systemstate
utmpdump /var/log/wtmp > $OUTPUT_DIR/wtmp.txt 2>&1
utmpdump /var/log/utmp > $OUTPUT_DIR/utmp.txt 2>&1
journalctl -a -o json-pretty > $OUTPUT_DIR/journalctl_json.txt 2>&1
journalctl -a > $OUTPUT_DIR/journalctl.txt 2>&1
journalctl -a -p 4 > $OUTPUT_DIR/journalctl_warn_plus.txt 2>&1



#proccess stuff
pretty_print INFO "Collecting Processes Data..."
ps > $OUTPUT_DIR/proc/ps.txt 2>&1
ps auxwww > $OUTPUT_DIR/proc/ps_auxwww.txt 2>&1
ps -deaf > $OUTPUT_DIR/proc/ps_deaf.txt 2>&1
ps -ef > $OUTPUT_DIR/proc/ps_ef.txt 2>&1
ps -efl > $OUTPUT_DIR/proc/ps_efl.txt 2>&1
ps -eo pid,etime,args > $OUTPUT_DIR/proc/ps_etime-args.txt 2>&1
ps -eo pid,lstart,args > $OUTPUT_DIR/proc/ps_lstart-args.txt 2>&1
pstree > $OUTPUT_DIR/proc/pstree.txt 2>&1
top -b -n1 > $OUTPUT_DIR/proc/top_bn1.txt 2>&1

for LOCAL_PROC_DIRECTORY in /proc/[0-9]*; do
    LOCAL_PROC_PID=`echo "$LOCAL_PROC_DIRECTORY" | cut -d"/" -f 3`
    if [ ! -z "$LOCAL_PROC_PID" ]; then
        mkdir -p $OUTPUT_DIR/proc/$LOCAL_PROC_PID
        cat /proc/$LOCAL_PROC_PID/comm > $OUTPUT_DIR/proc/$LOCAL_PROC_PID/comm.txt 2>&1
        strings /proc/$LOCAL_PROC_PID/cmdline > $OUTPUT_DIR/proc/$LOCAL_PROC_PID/cmdline.txt 2>&1
        strings /proc/$LOCAL_PROC_PID/environ > $OUTPUT_DIR/proc/$LOCAL_PROC_PID/environ.txt 2>&1
        cat /proc/$LOCAL_PROC_PID/maps > $OUTPUT_DIR/proc/$LOCAL_PROC_PID/maps.txt 2>&1
        cat /proc/$LOCAL_PROC_PID/task/$LOCAL_PROC_PID/children > $OUTPUT_DIR/proc/$LOCAL_PROC_PID/children.txt 2>&1
        ls -la /proc/$LOCAL_PROC_PID/fd > $OUTPUT_DIR/proc/$LOCAL_PROC_PID/fd.txt 2>&1
        readlink /proc/$LOCAL_PROC_PID/exe > $OUTPUT_DIR/proc/$LOCAL_PROC_PID/exe.txt 2>&1
        readlink /proc/$LOCAL_PROC_PID/cwd > $OUTPUT_DIR/proc/$LOCAL_PROC_PID/cwd.txt 2>&1
    fi
done

pretty_print INFO "Collecting Memory Data..."

# mem stuff
free -h >> $OUTPUT_DIR/mem/free.txt 2>&1
vmstat >> $OUTPUT_DIR/mem/vmstat.txt 2>&1

pretty_print INFO "Collecting Network Data..."

#net stuff
ifconfig -a > $OUTPUT_DIR/net/ifconfig.txt 2>&1
arp -a > $OUTPUT_DIR/net/arp.txt 2>&1
lsof -n -P -l -i > $OUTPUT_DIR/net/lsof_n_P_l_i.txt 2>&1
netstat -a > $OUTPUT_DIR/net/netstat_a.txt 2>&1
netstat -a -n > $OUTPUT_DIR/net/netstat_a_n.txt 2>&1 
netstat -i > $OUTPUT_DIR/net/netstat_i.txt 2>&1
netstat -r > $OUTPUT_DIR/net/netstat_r.txt 2>&1
netstat -r -n > $OUTPUT_DIR/net/netstat_r_n.txt 2>&1
ip addr show > $OUTPUT_DIR/net/ip_address.txt 2>&1
ss -a -n -p > $OUTPUT_DIR/net/ss_a_n_p.txt 2>&1
ss -a -p > $OUTPUT_DIR/net/ss_a_p.txt 2>&1
ip route show > $OUTPUT_DIR/net/ip_route_show.txt 2>&1
cat /etc/resolvconf/run/resolve.conf > $OUTPUT_DIR/net/resolvd.conf 2>&1
cat /etc/resolv.conf > $OUTPUT_DIR/net/resolv.conf 2>&1
cat /etc/dnsmasq.conf > $OUTPUT_DIR/net/dnsmasq 2>&1


pretty_print INFO "Collecting User Data..."

#user stuff
lastlog > $OUTPUT_DIR/user/lastlog.txt 2>&1
last > $OUTPUT_DIR/user/last.txt 2>&1
last -a -F > $OUTPUT_DIR/user/last_a_f.txt 2>&1
last -i > $OUTPUT_DIR/user/last_i.txt 2>&1
w > $OUTPUT_DIR/user/w.txt 2>&1
who > $OUTPUT_DIR/user/who.txt 2>&1
cat /var/log/auth.log > $OUTPUT_DIR/user/authlog.txt 2>&1
cat /var/log/auth.log | grep -v cron | grep -v sudo | grep -i user > $OUTPUT_DIR/user/authlog_filtered.txt 2>&1
cat /var/log/auth.log | grep -i command > $OUTPUT_DIR/user/authlog_sudo.txt 2>&1
grep cron /var/log/syslog > $OUTPUT_DIR/user/cron_log.txt 2>&1
cat /etc/passwd | cut -d : -f 1 > $OUTPUT_DIR/user/users.txt 2>&1
cat /etc/group > $OUTPUT_DIR/user/groups.txt 2>&1

#important file changes
stat /etc/passwd >> $OUTPUT_DIR/user/user_edits.txt 2>&1
stat /etc/group >> $OUTPUT_DIR/user/user_edits.txt 2>&1



pretty_print INFO "Collecting Disk Data..."

#disk stuff
df -h > $OUTPUT_DIR/disk/df.txt 2>&1
lsblk > $OUTPUT_DIR/disk/lsblk.txt 2>&1


pretty_print INFO "Downloading Volatility..."

#downloading volatility
TMPDIR=tmp_$(date | sha256sum | base64 | head -c 10)
CUR_DIR=$(pwd)
PROFILE=$(lsb_release -i -s)_$(uname -r).zip

git clone https://github.com/volatilityfoundation/volatility.git /tmp/$TMPDIR/volatility > /dev/null 2>&1


pretty_print INFO "Building Memory Profile..."

cd /tmp/$TMPDIR/volatility/tools/linux/ && make > /dev/null 2>&1
cd $CUR_DIR
zip $OUTPUT_DIR/$PROFILE /tmp/$TMPDIR/volatility/tools/linux/module.dwarf /boot/System.map-$(uname -r) > /dev/null 2>&1

pretty_print INFO "Memory Profile Built."

pretty_print INFO "Downloading AVML..."

wget https://github.com/microsoft/avml/releases/download/v0.2.1/avml -O /tmp/$TMPDIR/avml > /dev/null 2>&1
chmod +x /tmp/$TMPDIR/avml

pretty_print INFO "Dumping Memory - This might take a few."

/tmp/$TMPDIR/avml $OUTPUT_DIR/memdump.lime > /dev/null 2>&1

pretty_print YELLOW "All evidence collected and saved to $OUTPUT_DIR"
